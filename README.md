# Aircnc

Comp3900 group project - Accommodation Web Portal

## Installation / Build

### Backend

Install script:
```
$ git clone https://ZacCui@bitbucket.org/cs3900aircnc/aircnc.git
$ cd ./aircnc/aircnc
$ sh backend_install.sh
```
Uninstall script:
```
$ sh uninstall.sh
```
Install step by step without using `Install script` (**virtualenv**):
```
$ cd ./aircnc/aircnc
$ pip3 install -r requirements.txt
```
Install Database
```
$ python3 manage.py makemigrations
$ python3 manage.py migrate
```
Insert fake data **(Need install database first)**
```
$ python3 manage.py generate_user <num_users>
$ python3 manage.py populate_db # insert 3 houses for each user by default
```
Run
```
$ python3 manage.py runserver
```
The backend will run at localhost:8000

Testing
```
$ python3 manage.py test
```
### Frontend

Step by step:

Install npm and then install npm module dependencies:

```
$ git clone https://ZacCui@bitbucket.org/cs3900aircnc/aircnc-frontend.git
$ cd ./aircnc-frontend
$ yarn install 
```
For automatic building during development run:

```
$ yarn start
```
The frontend will run at localhost:3000

## Dependencies

### Frontend Dependencies
|Package Name|Version|
|---|---|
|antd|3.8.0|
|axios|0.18.0|
|firebase|5.5.3|
|jquery|3.3.1|
|lodash|4.17.10|
|moment|2.22.2|
|querystringify|2.0.0|
|react|16.4.1|
|react-dom|16.4.1|
|react-facebook-login|4.1.1|
|react-infinite-scroller|1.2.0|
|react-redux|5.0.7|
|react-router-dom|4.3.1|
|react-scripts|1.1.4|
|redux|4.0.0|
|redux-thunk|2.3.0|


### Backend Dependencies
|Package Name|Version|
|---|---|
|Django|2.0.7|
|django-cors-headers|2.4.0|
|django-filter|2.0.0|
|djangorestframework|3.8.2|
|djangorestframework-jwt|1.11.0|
|PyJWT|1.6.4|
|Pillow|5.2.0|
|social-auth-app-django|2.1.0|
|django-rest-framework-social-oauth2|1.1.0|
|django-oauth-toolkit|1.2.0|
|django-filter|2.0.0|
|geopy|1.16.0|
|django-secure|1.0.1|
