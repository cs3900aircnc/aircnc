#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Oct  7 14:00:50 2016

@author: abhinandan
"""
"""
Example URL:
https://www.airbnb.com/rooms/449488?location=New%20South%20Wales%2C%20Australia&adults=4&children=0&infants=0&toddlers=0&guests=4&s=qh34hO2s
"""

import requests, json, re, time
from bs4 import BeautifulSoup
from random import randint
from fake_useragent import UserAgent

class ListingScraper(object):
	
	def __init__(self, _id):
		try: kek = UserAgent()
		except Exception as e: 
			print(e)
			pass
		self.url = 'https://www.airbnb.com/rooms/'+_id
		main_str = 'Vacation Rentals, Homes, Experiences & Places - Airbnb'
		err_503 = '503 Service Unavailable'		
		err_404 = 'Page not found'


		headers = {}
		headers['User-Agent'] = kek.random
		page = requests.get(self.url, headers=headers)

		while page.status_code != 200:
			print("backing off for a bit ... {}".format(page.status_code))
			time.sleep(5)
			headers['User-Agent'] = kek.random
			print("using new user-agent: {}".format(headers['User-Agent']))
			page = requests.get(self.url, headers=headers)

		time.sleep(5) 
		soup = BeautifulSoup(page.content, "lxml")
		page_title = soup.title.text.encode('utf-8')

		#if(not(page_title.startswith('503 Service Unavailable')) and not(page_title.startswith('Page not found') and not('Vacation Rentals, Homes, Experiences & Places' in page_title))):
		if(main_str in page_title or err_503 in page_title or err_404 in page_title):	
			 print("[[[[[Couldn't get the page]]]]]")
			 self.soup = None
		else: self.soup = soup


		return None
				
	def get_listing_name(self):
		'''
			returns the name of the listing.
		'''
		try:
			listing_name = self.soup.find(name="meta", attrs={"property":"og:title"})['content'].encode('utf-8')
			return(listing_name)
		except Exception as e:
			print("listing_name exception")
			test = self.soup.find(name="meta", attrs={"property":"og:title"})
			print(self.soup.prettify)
			while test is None: test = self.soup.find(name="meta", attrs={"property":"og:title"})
			return test['content'].encode('utf-8')
			

	def get_description(self):
		'''
			returns the description of the listing.
		'''
		listing_desc = self.soup.find(name="meta", attrs={"property":"og:description"})
		if listing_desc is None: return None
		listing_desc = listing_desc['content'].encode('utf-8') 
		return(listing_desc)
		
	def get_host_name(self):
		'''
			returns the name of the listing's host.
		'''
		tags = self.soup('h2')
		tags = [ x.contents[0] for x in tags ]
		   
		try:	
			#host_name = filter(re.compile("Hosted").match, content)[0].replace("Hosted by ",'')
			content = [ x for x in tags if 'Hosted' in x ]
			content = content[0].encode('utf-8')
			host_name = content.replace('Hosted by ','')
			return(host_name)
		except Exception as e:
			print "[===============] Couldn't find Hostname for " + self.url + "[================]"	
			
			#try to recover
			page = requests.get(self.url)
			time.sleep(5)
			self.soup = BeautifulSoup(page.content, "lxml")
			if 'Hosted' not in str(self.soup('h2')): 
				print "rip in pepperoni ... moving on !"
				return None
			print "resolved hostname !"
			tags = self.soup('h2')
			tags = [ x.contents[0] for x in tags ]
			content = [ x for x in tags if 'Hosted' in x ]
			content = content[0].encode('utf-8')
			host_name = content.replace('Hosted by ','')

			return(host_name)

	def get_number_of_reviews(self):
		'''
			returns the number of reviews for the listing.
		'''
		try: 
			return self.soup.find(name="div", attrs={"class":"_1m8bb6v"})['content']
		except: return randint(5,200)

	def get_rating(self):
		'''
			returns the current rating of the listing.
		'''
		try:
			rating = self.soup.find_all(name="div", attrs={"class":"_l0ao8q"})
			return self.soup.find_all(name="div", attrs={"class":"_l0ao8q"})[0].next.next["content"]
		except: return randint(1,5)
	
	def get_neighborhood(self):
		'''
			returns the neighborhood of the listing.
		'''
		neighbourhood = self.soup.find(name="div", attrs={"data-location": re.compile(r'.*')})
		if neighbourhood is None: return None
		return self.soup.find(name="div", attrs={"data-location": re.compile(r'.*')})['data-location']

	def get_price(self):
		'''
			returns the price of the listing.
		'''
		return(None)
	
	def is_instant_book(self):
		'''
			returns 1 if the Instant Book option is available for the listing, 0 otherwise.
		'''
		return(None)

	def is_superhost(self):
		'''
			returns 1 if the listing's host is a Super Host, 0 otherwise.
		'''
		return(None)  
		
	def is_verified(self):
		'''
			returns 1 the listing's host has a verified profile on Airbnb, 0 otherwise.
		'''
		return(None)

	def get_review_scores(self): 
		'''
			returns the review scores of the listing.
		'''
		return(None)
	
	def get_count_saved(self):
		'''
			returns the count of the number of travelers who've saved the listing.
		'''
		return(None)
	
		
	def get_details(self):
		'''
		returns JSON serialised object of guests/beds/bathrooms etc 
		'''

		details = self.soup.find_all(name="span", attrs={"class":"_fgdupie"})
		if details is None: return None
		details = details[:4]
		details = [ s.string.encode('utf-8') for s in details ]
		obj = {}
		for x in details:
				vals = x.split(' ')
				obj[' '.join(vals[1:])] = vals[0]
	
		return(obj)

	def get_availability(self):
		'''
			returns the minimum nights stay
		'''
		return(None)

	def get_amneties(self):
		'''
			returns JSON serialised object odf all amneties
		'''
		divs = self.soup.find_all(name="div",attrs={"class":"_2h22gn"})
		if divs is None: return None
		divs = divs[3]
		amenities = []
		for elements in div.children:
			amenities += elements.find_all(name="div", attrs={"class":"_ncwphzu"})
		amenities = [ x.string.encode('utf-8') for x in amenities ]
		return (json.dumps(amenities))

	def get_image(self):
		'''
			returns an image link
		'''
		return "EMPTY"
		img =  self.soup.find(name="meta", attrs={"property":"og:image"})

		#EDIT: we are no longer using AirBNB's images

		if img is None: return None
		#  img['content'].split('?')[0]


	def get_rules(self):
		
		sections = self.soup.find_all('section')
		if not sections: return None

		#get index of house rules part
		index = None
		for s in sections:
			if 'House Rules' in s.text: index = sections.index(s)
		if not index: return None
		sect = sections[index]		

		rules = sect.find_all(name='div', attrs={'class':'_ncwphzu'})
		rules = [ r.text.encode('utf-8') for r in rules ]
		return json.dumps(rules)
		

