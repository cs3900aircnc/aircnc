#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sun Oct  9 19:35:16 2016

@author: abhinandan
"""
import argparse
from random import randint
from AirbnbScrape import ListingScraper
 
def scrape_data(_id):
    d = {}
    scraper = ListingScraper(str(_id))
    if(scraper.soup is not None):
#        d['url'] = scraper.url
#        d['listing_id'] = _id
        d['listing_name'] = scraper.get_listing_name()
        d['listing_description'] = scraper.get_description()
        d['host_name'] = scraper.get_host_name()
        d['number_of_reviews'] = scraper.get_number_of_reviews()
        d['rating'] = scraper.get_rating()
#        d['price'] = scraper.get_price()
        d['neighborhood'] = scraper.get_neighborhood()
#        d['verified'] = scraper.is_verified()
#        d['superhost'] = scraper.is_superhost()
#        d['instant_book'] = scraper.is_instant_book()
#        d['count_saved'] = scraper.get_count_saved()
#        d['availability'] = scraper.get_availability()

#        d['details'] = scraper.get_details()
        details = scraper.get_details()
        #print details
        if 'guests' in details.keys():
            num_guests = details['guests'].replace('+','')
            d['guests'] = int(num_guests)
        else:
            d['guests'] = randint(1,10)

        key = [ x for x in details.keys() if 'bed' in x ]
        if key == [] or type(key[0]) is str: d['beds'] = randint(1,10)
        else:	d['beds'] = int(details[key[0]])

        key = [ x for x in details.keys() if 'bedroom' in x ]
        if key == [] or type(key[0]) is str: d['bedrooms'] = randint(1,10)
        else:	d['bedrooms'] = int(details[key[0]])

        key = [ x for x in details.keys() if 'bath' in x ]
        if key == [] or type(key[0]) is str: d['baths'] = randint(1,10)
        else: d['baths'] = int(round(float(details[key[0]])))

        d['image'] = scraper.get_image()        #will be none
        d['rules'] = scraper.get_rules()
        d['id'] = _id
        
        return(d)
    else:
        return(None)
