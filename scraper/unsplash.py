#!/usr/bin/python

import requests, json
from time import sleep, time

KEY='16415e5560b0df81fbd94cb64f1586dd3a43fa5e3bacd0eba848353313744400'
SEC='55c079977ae01abd7c0a3284dd22c827265493cb579741e610c9a7b091b0c683'

KEY2='44ed3d6a344416866a42f6748adda1fde92f14655d6b53157e002ee9591f86fb'
SEC2='cbc20a6272f41621ad0222cdab0dda701ad2d4f29200be53fed65154d0be413b'

KEY3='95ab10d17c75301a7aa164493f4b3af2c5ec177cd05b55aa3d62b6c6907c4f41'
SEC3='2caf11ed494b90a90087c8bcccebd4d50296eaeea2bc11c49bc8eff163e58c22'

KEY4=''

base_url="https://api.unsplash.com/search/photos?client_id=" + KEY4 + "&query=rooms%20interior&per_page=30"

# create new file if empty otherwise let it be 
try: f = open('imagelinks').close()
except: f = open('imagelinks', 'w+').close()

# loop through this range
for x in range(366,446):
    url = base_url+"&page="+str(x)
    print("Getting: %s" % url)
    r = requests.get(url)

    # wait for rate limit cooldown
    start = time()
    while r.status_code != 200:
        elapsed = (time() - start) // 60 % 60 # in minutes 
        print "*** %d (sleeping) (time elapsed: %s minutes) ***" % (r.status_code, elapsed)
        print r.text
        sleep(15*60)
        r = requests.get(url)
        
    # json returned 
    data = json.loads(r.text)
    try:
        for r in data['results']:           # raw might impact performance
            if 'regular' in r['urls'].keys(): img_url = r['urls']['regular']
            else: img_url = r['urls']['full']

            # append link to file
            with open('imagelinks', 'a') as f:
                f.write(img_url + '\n')


    except Exception as e:
        print "Skipping %s (%s)" % (url, e)
        continue
