#!/usr/bin/env python

from re import sub
from os import getpid
from re import search
from time import sleep
from random import randint
from selenium import webdriver
from pyvirtualdisplay import Display
from selenium.webdriver.common.by import By 
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait, Select
from selenium.webdriver.support import expected_conditions as EC 

class LinkSpider():
	def __init__(self):								
		self.url_to_crawl = "https://www.airbnb.com/s/New-South-Wales--Australia/homes?refinement_paths[]=%2Fhomes&allow_override[]=&"
		self.all_items = []

	# Open headless chromedriver
	def start_driver(self):
		print('starting driver...')
		self.display = Display(visible=0, size=(800, 600))
		self.display.start()

		options = webdriver.ChromeOptions()
		options.add_argument(" - incognito")
		options.add_argument('--disable-extensions')
		options.add_argument('--no-sandbox')
		options.add_argument('--headless')
		options.add_argument('--disable-gpu')
		options.add_argument('--disable-setuid-sandbox')
		options.add_argument('--allow-running-insecure-content')
		options.add_argument('--ignore-certificate-errors')
	
		# don't load images // use cache
		prefs={"profile.managed_default_content_settings.images": 2, 'disk-cache-size': 4096 }
		options.add_experimental_option("prefs", prefs)

		# make sure you have a chromedriver-executable installed, either in path or give the path
		# exit staus 127 probably means some libraries are missing, try running it to see what it is 
		# sudo apt install libgconf2-4 might help 

		try: 
			print('attempting to start chrome...')
			self.driver = webdriver.Chrome(executable_path="/var/chromedriver/chromedriver", chrome_options=options)
			self.driver.set_page_load_timeout(10)
		except Exception as e:
			print('couldn\'t launch chrome')
			print(str(e))
			self.display.stop()
			self.driver.quit()
	
	# Close chromedriver
	def close_driver(self):
		print('closing driver...')
		self.display.stop()
		self.driver.quit()
		print('closed!')

	# Tell the browser to get a page
	def get_page(self, url):

		try:
			self.driver.get(url)
			return True
		except TimeoutException:
			print("[SKIPPING *] Timed out on loading: %s" % url)
			return False

			#self.close_driver()
			#exit(1)


	def grab_list_items(self):

		###################		
		extract_amount = 100	
		###################

		count = 0
		pause_time = 2

		while count < extract_amount:
			try:	

                #implement exponential backoff time
				if(self.all_items): pause_time = len(self.all_items) / 4
				print("\n>>> Try loading page first ... [%d/%d] (Sleeping %d)" % (count, extract_amount, pause_time))
				sleep(pause_time)

				#load the div elements and scrape the links
				for c, div in enumerate(self.driver.find_elements_by_class_name('_1szwzht')):
					#print(div.text)
					try: data = div.find_element_by_css_selector('._1szwzht a').get_attribute('href') 
					except Exception as e:
						print(str(e)) 
						pass
					if data and 'plus' not in data:
						self.all_items.append(data)
						count += 1
					else:
						pass

				#click to go to next page 
				# there are four elements with class "_1ip5u88", we want the last one which is the ">" button
				buttons = self.driver.find_elements_by_class_name('_1ip5u88')
				print(">>> clicking next ...")
				buttons[2].click()
					

			except Exception as e:
				print("Exception occured in button-pressing loop")
				print(e)
				print("Skipping this URL entirely [*]")
				return 
				#self.close_driver()
				#exit(1)

	def parse(self, url):
		self.start_driver()

		print("="*160)
		print("[ NOW SCRAPING * ]: %s" % url)
		print("="*160)
		result = self.get_page(url)
		if result is False: return False
		self.grab_list_items()

		items_list = list(set(self.all_items))
		items_list = [ search("/(\d+)\?",item).group(1) for item in items_list]
			
		#offload the items to file
		with open('/tmp/links', 'a') as f:	
			for i in items_list:
				f.write(str(i)+'\n')				

		#empty it back again	
		self.all_items = []
		self.close_driver()     #maybe this might work ...

		if self.all_items:
			return self.all_items
		else:
			return False, False

#======================= MAIN ====================================		
with open("./ids", 'r') as ids: id_list = ids.read().splitlines()

if not id_list:
	print("Couldn't find/parse id file !")
	exit(1)

# opened file containing timed-out links to retest them ! 
try: m = open("./missed", 'r')
except: m = None

# Run spider
Link = LinkSpider()

if m:
	missed = m.read().splitlines()
	print("[Rerunning for missing links]")
	for url in missed:
		s = Link.parse(url)

	print("[Check output again for missing links to be safe !]")
	m.close()
	exit(0)

# Create the empty file to store in
open('/tmp/links' , 'w').close()

# loop through the suburb ids
for id in id_list:
	url = Link.url_to_crawl + "neighbourhood_ids[]=" + id
	
	#loop through number of guests that can stay 
	for guests in range(1,17, 2):
		url = sub("&adults=.*",'',url)			
		url += "&adults=" + str(guests)
		
		sleep(2)	#no DDOS pls
		items_list = Link.parse(url)

print("================ DONE ===============")

#=====================================================================
