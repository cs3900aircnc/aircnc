#!/usr/bin/python

import requests, json, re, time, sys
from os import system, getpid
from bs4 import BeautifulSoup
from random import randint, choice, normalvariate
import datetime as dt

# Stage 1: Uncomment the code below otherwise stick to stage 2
'''
suburbs_url = "https://geographic.org/streetview/australia/nsw/"

# Scrape Suburbs -> Filenames
# Scrape Streets -> Save under Suburb

print("This page takes awhile ... wait 10 seconds")
r = requests.get(suburbs_url)
soup = BeautifulSoup(r.content,'lxml')

l = soup("li")

d = {}
for x in l:
	tag = x("a")[0]	 # ie. dict['randwick.html'] = "Randwick"
	d[ tag['href'] ] = tag['alt']  


for x in d.keys():
	
	sub = suburbs_url + x
	filename = d[x]
	print("Processing: " + filename)

	with open(filename, 'w+') as f:
		page = requests.get(sub)
		psoup = BeautifulSoup(page.content,'lxml')

		m = psoup("li")
		for x in m:
			tag = x("a")[0]
			address = tag.text.encode('utf-8')
			f.write(address + "\n")
'''
# Stage 2
# Parse in JSON strings and add "Fake addresses" to each JSON dict and resave it to file

print("[Output will be stored in /tmp/json_out" + str(getpid()) + "]")
outfile = "/tmp/json_out" + str(getpid())
try:
	f = open('/tmp/json_in','r')
	f.close()
except Exception as e:
	print("***Please place your input file as /tmp/json_in***")
	exit(1)

print("[Unpacking and setting up files in /tmp ... ]")
# unzip the suburbs.zip into folder
try:
	system("unzip suburbs.zip -d /tmp > /dev/null")
	system("ls /tmp/subs > /tmp/sublist")
except Exception as e:
	print str(e)
	print("***Couldn't unzip -- Fatal Exit***")
	exit(1)

# Load up the pictures into memory
try:
	with open("final_images",'r') as f:
		piclist = f.readlines()
		piclist = [ x.rstrip() for x in piclist ]
except Exception as e:
	print str(e)
	print("***Please place image link file as ./final_images***")
	exit(1)

# Read all the filenames (will use for differentiation later)
with open("/tmp/sublist", 'r') as f:
	suburbs_list = [ line.rstrip() for line in f ]

# Assumes that the files are stored in a directory "suburbs"
def rng(suburb):
	global suburbs_list
	if suburb not in suburbs_list:
		print(".:: Can't find %s ::." % suburb)
		return "NOT FOUND"

	if(suburb == 'Sydney'): suburb = "Sydney Central Business District"	
	#get street names
	with open("/tmp/subs/" + suburb,'r') as s:
		streets = s.read().splitlines()
	address = choice(streets)

    #EDIT: adding in NSW for geolocation purposes
	address = str(randint(1,20)) + " " + address + " NSW"
	print("Assigned [%s]: %s " % (suburb, address))
	
	return address

# Generates a date range  (today, 1 year later)
def gen_date():
	today = dt.date.today()
	start_date = today + dt.timedelta(days=randint(1,110))
	#duration = randint(1,14)
	duration = 365
	end_date = start_date + dt.timedelta(days=duration)
	print "Available from %s - %s" % (str(start_date), str(end_date))
	return [ str(start_date), str(end_date) ]

# Return a list of four images 
def select_images():

    #use global piclist
	ret = []
	for x in range(4): ret.append(piclist.pop())
	return ret 

# Open the JSON file
with open("/tmp/json_in", 'r') as f: 
	l = f.readlines()

with open(outfile, 'w+') as f: f.close()
# Now all the dicts are in memory
for x in l:

	try: d = json.loads(x)
	except: continue		# that line isnt a JSON dump 
	suburb = d['neighborhood'].lstrip().rstrip()

	d['address'] = rng(suburb)
	if d['address'] == "NOT FOUND": continue 	# skip non-NSW properties
	dates = gen_date()
	d['available_dates'] = dates

	#round(nv(1,5) % 5,2)
	value = round(normalvariate(1,5) % 5,2)
	if value < 1: value += 1
	d['rating'] = value

    # replace None with a list of 4 images
	d['image'] = select_images()

	with open(outfile, 'a+') as f:
		f.write(json.dumps(d)+ "\n")

# clean up the files
print("[Cleaning up the companion files]")
try: system("rm -rf /tmp/subs /tmp/sublist")
except: print("Couldn't clean up /tmp files !!!")

