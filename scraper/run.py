import argparse, sys, json
from time import sleep
from scrape import scrape_data

if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument("-i", required=False, help="Airbnb listing id")
	args = parser.parse_args()

	if args.i:
		print("Running for a single ID")
		res = scrape_data(args.i)
		print(json.dumps(res))

		sys.exit(1)
    
	if	sys.stdin.isatty():
		print("Usage: " + sys.argv[0] + " < [FILENAME]")
		sys.exit(1)

	for line in sys.stdin:
		id = int(line)

		#cooldown to avoid getting blocked
		sleep(7) #usually set it to 20
		
		res = scrape_data(id)
		if res is None: continue
		if 'null' in res.values() or None in res.values(): sys.stdout.write('[FAULTY SCRAPE] ---> ')
		print(json.dumps(res))			

	print("================ DONE ========================")
