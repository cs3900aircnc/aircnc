from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator

# Create your models here.


class Booking(models.Model):
    booking_status = (
        ('C', 'Completed'),
        ('P', 'Pending'),
        ('A', 'Approved'),
        ('R', 'Rejected'),
    )
    host = models.ForeignKey('account.User', on_delete=models.CASCADE, related_name='host',
                             verbose_name="host")

    guest = models.ForeignKey('account.User', on_delete=models.CASCADE, related_name='guest',
                              verbose_name="guest")

    house = models.ForeignKey('housing.Housing', related_name='bookings',
                              on_delete=models.CASCADE, verbose_name="house_bookings")

    price = models.IntegerField()

    startDate = models.DateTimeField()

    endDate = models.DateTimeField()

    status = models.CharField(
        max_length=2,
        choices=booking_status,
        default='P',
    )

    message = models.CharField(max_length=1000, default='')

    rating = models.PositiveSmallIntegerField(blank=True, null=True, validators=[
                                              MinValueValidator(0), MaxValueValidator(5)])
