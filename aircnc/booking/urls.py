from django.urls import path, include, re_path
from rest_framework import routers
from .views import BookingViewSet

router = routers.DefaultRouter()
router.register(r'', BookingViewSet)

urlpatterns = [
    re_path(r'', include(router.urls)),
]
urlpatterns += router.urls
