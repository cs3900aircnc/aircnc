from rest_framework import viewsets
from .serializers import BookingSerializer, UserBookingSerializer
from rest_framework import permissions, status
from .models import Booking
from housing.models import Housing
from account.models import User
from rest_framework.response import Response
from rest_framework.decorators import action
from datetime import datetime
from pytz import timezone
from django.utils import timezone as tz
from django.db.models import Q
from django.template.loader import render_to_string
from django.core.mail import send_mail


class BookingViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    # permission_classes = (permissions.AllowAny,)

    queryset = Booking.objects.all()
    serializer_class = BookingSerializer

    def create(self, request):

        if request.user.id != request.data.get('guest'):
            error = {
                'error': 'You cannot book this house for other users'
            }
            return Response(error, status=status.HTTP_400_BAD_REQUEST)

        houseId = request.data.get('house')
        housings = Housing.objects.filter(id=houseId)
        From = housings[0].startDate
        To = housings[0].endDate
        start = request.data.get('startDate')
        end = request.data.get('endDate')

        if not inTheDateRange(From, To, start, end) or hasConflicts(start, end, houseId):
            error = {
                'error': 'The house is unavaible during the request date'
            }
            return Response(error, status=status.HTTP_400_BAD_REQUEST)
        self.email('You have a new order to be comfirmed!',
                   housings[0].user.username, housings[0].user.email, request.user.username, 'requested', 'host')
        return super().create(request)

    @action(methods=['get'], detail=False)
    def my(self, request, *args, **kwargs):
        bookings = Booking.objects.filter(
            Q(host=request.user) | Q(guest=request.user))
        checkOrderStatus(request.user)
        serializer = UserBookingSerializer(bookings, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @action(methods=['get'], detail=False)
    def asHost(self, request, *args, **kwargs):
        bookings = Booking.objects.filter(Q(host=request.user))
        checkOrderStatus(request.user)
        serializer = UserBookingSerializer(bookings, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @action(methods=['get'], detail=False)
    def asGuest(self, request, *args, **kwargs):
        bookings = Booking.objects.filter(Q(guest=request.user))
        checkOrderStatus(request.user)
        serializer = UserBookingSerializer(bookings, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @action(methods=['post'], detail=True)
    def rate(self, request, pk=None):
        booking = Booking.objects.get(pk=pk)

        if request.user.id != booking.guest.id:
            error = {
                'error': 'You cannot rate this house'
            }
            return Response(error, status=status.HTTP_400_BAD_REQUEST)

        if booking.status != 'C':
            error = {
                'error': 'The order is not completed yet'
            }
            return Response(error, status=status.HTTP_400_BAD_REQUEST)

        serializer = BookingSerializer(
            booking, data={'rating': request.data.get('rating')}, partial=True)

        if serializer.is_valid():
            serializer.save()
            updateRating(booking.house.id)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(methods=['post'], detail=True)
    def complete(self, request, pk=None):
        booking = Booking.objects.get(pk=pk)
        if request.user.id != booking.guest.id or booking.status != 'A':
            error = {
                'error': 'You do not have the permission for this operation'
            }
            return Response(error, status=status.HTTP_400_BAD_REQUEST)

        serializer = BookingSerializer(
            booking, data={'status': 'C'}, partial=True)

        if serializer.is_valid():
            serializer.save()
            self.email('You order has been completed!',
                       booking.host.username, booking.host.email, request.user.username, 'completed', 'host')
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(methods=['post'], detail=True)
    def approve(self, request, pk=None):
        booking = Booking.objects.get(pk=pk)
        if request.user.id != booking.host.id or booking.status != 'P':
            error = {
                'error': 'You do not have the permission for this operation'
            }
            return Response(error, status=status.HTTP_400_BAD_REQUEST)

        serializer = BookingSerializer(
            booking, data={'status': 'A'}, partial=True)

        if serializer.is_valid():
            serializer.save()
            self.email('You order has been processed!',
                       booking.guest.username, booking.guest.email, request.user.username, 'approved', 'guest')
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(methods=['post'], detail=True)
    def reject(self, request, pk=None):
        booking = Booking.objects.get(pk=pk)
        if request.user.id != booking.host.id or booking.status != 'P':
            error = {
                'error': 'You do not have the permission for this operation'
            }
            return Response(error, status=status.HTTP_400_BAD_REQUEST)

        serializer = BookingSerializer(
            booking, data={'status': 'R'}, partial=True)

        if serializer.is_valid():
            serializer.save()
            self.email('You order has been been processed!',
                       booking.guest.username, booking.guest.email, request.user.username, 'rejected', 'guest')
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def email(self, title, reciver, email, sender, action, role):
        html_message = render_to_string('booking_template.html', {
            'username': reciver,
            'url': "https://aircnc-ac643.firebaseapp.com/mybooking/" + role,
            'action': action,
            'sender': sender
        })
        try:
            send_mail(title, "", 'admin@aircnc.com', [
                email], fail_silently=False, html_message=html_message)
        except:
            print('Failed to send email')
            pass


def updateRating(houseId):
    bookings = Booking.objects.filter(
        Q(house=houseId) & Q(status='C')).exclude(rating=None)
    if len(bookings) == 0:
        return
    num_of_rating = 0
    sum_of_rating = 0
    for order in bookings:
        sum_of_rating += order.rating
        num_of_rating += 1
    house = Housing.objects.get(id=houseId)
    house.rating = round(sum_of_rating / num_of_rating, 1)
    house.save()


def inTheDateRange(range_from, range_to, start, end):
    start = datetime.strptime(start, "%Y-%m-%dT%H:%M:%SZ")
    end = datetime.strptime(end, "%Y-%m-%dT%H:%M:%SZ")
    start = start.replace(tzinfo=timezone('UTC'))
    end = end.replace(tzinfo=timezone('UTC'))

    if range_from <= start and end <= range_to:
        return True
    return False


def dateConflicted(range_from, range_to, start, end):
    start = datetime.strptime(start, "%Y-%m-%dT%H:%M:%SZ")
    end = datetime.strptime(end, "%Y-%m-%dT%H:%M:%SZ")
    start = start.replace(tzinfo=timezone('UTC'))
    end = end.replace(tzinfo=timezone('UTC'))
    if range_from > end or range_to < start:
        return False
    return True


def getBookingDateRange(houseId):
    result = []
    bookings = Booking.objects.filter(Q(house=houseId) & Q(status='A'))
    for booking in bookings:
        result.append((booking.startDate, booking.endDate))
    return result


def hasConflicts(start, end, houseId):
    date_list = getBookingDateRange(houseId)
    for dates in date_list:
        if(dateConflicted(dates[0], dates[1], start, end)):
            return True
    return False


def checkOrderStatus(user):
    bookings = Booking.objects.filter(
        (Q(host=user) | Q(guest=user)) & Q(status='A'))
    now = tz.now()
    for order in bookings:
        if order.endDate < now:
            order.status = 'C'
            order.save()
