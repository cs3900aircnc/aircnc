from rest_framework import serializers
from .models import Booking
from account.models import User
from housing.models import Housing
from account.serializers import UserSerializer
from search.serializers import SearchHousingSerializer
from rest_framework.response import Response


class UserRelatedField(serializers.RelatedField):
    def to_representation(self, obj):
        return {
            'pk': obj.pk,
            'username': obj.username,
            'email': obj.email,
            'first_name': obj.first_name,
            'last_name': obj.last_name,
            'iconUrl': obj.iconUrl,
            'phone': obj.phone,
            'description': obj.description
        }


class BookingSerializer(serializers.ModelSerializer):

    class Meta:
        model = Booking
        fields = ('pk', 'host', 'guest', 'house', 'price',
                  'startDate', 'endDate', 'rating', 'status', 'message')


class UserBookingSerializer(serializers.ModelSerializer):
    host = UserSerializer(read_only=True)
    guest = UserSerializer(read_only=True)
    house = SearchHousingSerializer(read_only=True)

    class Meta:
        model = Booking
        fields = ('pk', 'host', 'guest', 'house', 'price',
                  'startDate', 'endDate', 'rating', 'status', 'message')


class BookingDateRangeSerializer(serializers.ModelSerializer):

    class Meta:
        model = Booking
        fields = ('startDate', 'endDate', 'status')
