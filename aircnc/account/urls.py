from django.urls import path, include, re_path
from . import views
from rest_framework import routers
from .views import current_user, register, changeIcon, updateEmail, updateName, update

router = routers.DefaultRouter()

urlpatterns = [
    re_path(r'', include(router.urls)),
    re_path(r'(?i)current_user/', current_user, name='current_user'),
    re_path(r'(?i)register/', register.as_view(), name='register'),
    re_path(r'(?i)changeIcon/', changeIcon.as_view(), name='change_icon'),
    re_path(r'(?i)update_name/', updateName.as_view(), name='update_name'),
    re_path(r'(?i)update_email/', updateEmail.as_view(), name='update_email'),
    re_path(r'(?i)updateProfile/', update.as_view())
]
