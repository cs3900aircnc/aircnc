from django.core.management.base import BaseCommand, CommandError
from account.models import User
from account.serializers import UserProfileSerializerWithToken
import requests


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('num_of_user', type=int)

    def handle(self, *args, **options):
        self.insert_users(options['num_of_user'])

    def insert_users(self, num_of_user):
        for i in range(num_of_user):
            data = self.get_user()
            user_data = {
                "username": data['login']['username'],
                "first_name": data['name']['first'],
                "last_name": data['name']['last'],
                "email": data['login']['username'] + "@aircnc.com",
                "password": "123456",
                "iconUrl": data['picture']['thumbnail']
            }
            user = UserProfileSerializerWithToken(data=user_data)
            if user.is_valid():
                user.save()
                print("Inserted User({}) with username({})".format(
                    user.data.get('id'), user.data.get('username')))
            else:
                print("Failed to insert user")
                print(user.errors)

    def get_user(self):
        return requests.get("https://randomuser.me/api/").json()['results'][0]
