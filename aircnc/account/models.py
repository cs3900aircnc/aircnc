from django.db import models
from django.contrib.auth.models import AbstractUser
from django.core.mail import send_mail
from django.contrib.auth.models import PermissionsMixin
from django.utils.translation import ugettext_lazy as _


class User(AbstractUser):
    email = models.EmailField(_('email address'), unique=True)
    # as we've decided store the img in frontend, so we don't use the icon anymore
    icon = models.ImageField(max_length=100)
    # For import data we only use iconUrl
    iconUrl = models.URLField()
    phone = models.CharField(max_length=20, default='', null=True, blank=True)
    description = models.CharField(
        max_length=1000, default='', null=True, blank=True)

    def email_user(self, subject, message, from_email=None):
        """
        Sends an email to this User.
        """
        send_mail(subject, message, from_email, [self.email])

    def __str__(self):
        s = "[{}] ({})".format(self.id, self.username)
        return s


# TODO change the email field to required
# https://docs.djangoproject.com/en/2.0/topics/auth/customizing/#using-a-custom-user-model-when-starting-a-project
