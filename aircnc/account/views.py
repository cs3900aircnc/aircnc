from django.http import HttpResponseRedirect
from .models import User
from rest_framework import permissions, status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.views import APIView
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.contrib.sites.shortcuts import get_current_site
from .serializers import UserSerializer, UserSerializerWithToken, UserProfileSerializerWithToken
from django.utils import timezone
from django.template.loader import render_to_string
from django.core.mail import send_mail
from aircnc.settings import EMAIL_HOST_USER


@api_view(['GET'])
def current_user(request):
    serializer = UserSerializer(request.user)
    return Response(serializer.data, status=status.HTTP_200_OK)


class register(APIView):
    permission_classes = (permissions.AllowAny,)

    def post(self, request, format=None):
        serializer = UserSerializerWithToken(data=request.data)
        if serializer.is_valid():
            serializer.save()

            user = User.objects.get(email=request.data['email'])
            url = get_current_site(request).domain
            html_message = render_to_string('acc_active_email.html', {
                'username': serializer.data['username'],
                'url': "https://aircnc-ac643.firebaseapp.com/",
            })
            try:
                send_mail('Thanks for join us', "", 'admin@aircnc.com', [
                    user.email], fail_silently=False, html_message=html_message)
            except:
                pass

            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class changeIcon(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, format=None):
        user = User.objects.get(id=self.request.user.id)
        serializer = UserSerializer(
            user, data={'iconUrl': request.data.get('iconUrl')}, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class update(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, format=None):
        user = User.objects.get(id=self.request.user.id)
        serializer = UserProfileSerializerWithToken(
            user, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


def save_profile(backend, user, response, *args, **kwargs):
    if backend.name == 'facebook':
        if response.get('picture').get('data').get('url') is not None:
            user.iconUrl = response.get('picture').get('data').get('url')
        user.last_login = timezone.now()
        user.save()


class updateName(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, format=None):
        user = User.objects.get(id=self.request.user.id)

        # if key not found in request, use existing values instead
        new_first_name = request.data.get('first_name', user.first_name)
        new_last_name = request.data.get('last_name', user.last_name)

        serializer = UserSerializer(
            user,
            data={'first_name': new_first_name, 'last_name': new_last_name},
            partial=True
        )

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_202_ACCEPTED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class updateEmail(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, format=None):
        user = User.objects.get(id=self.request.user.id)

        # if key not found in request, use existing values instead
        new_email = request.data.get('email', user.email)

        serializer = UserSerializer(
            user, data={'email': new_email}, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_202_ACCEPTED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
