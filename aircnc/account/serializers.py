from rest_framework import serializers
from rest_framework_jwt.settings import api_settings
from .models import User
from datetime import datetime
from django.utils import timezone


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('pk', 'username', 'email', 'first_name', 'last_name',
                  'iconUrl', 'phone', 'description')


class UserSerializerWithToken(serializers.ModelSerializer):

    token = serializers.SerializerMethodField()
    password = serializers.CharField(write_only=True)

    def get_token(self, obj):
        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

        payload = jwt_payload_handler(obj)
        token = jwt_encode_handler(payload)
        return token

    def create(self, validated_data):
        password = validated_data.pop('password', None)
        instance = self.Meta.model(**validated_data)
        if password is not None:
            instance.set_password(password)
        if validated_data.get('iconUrl') is not None:
            instance.iconUrl = validated_data.get('iconUrl')
        instance.last_login = timezone.now()
        instance.save()
        return instance

    class Meta:
        model = User
        fields = ('pk', 'token', 'username', 'email', 'password')


class UserProfileSerializerWithToken(serializers.ModelSerializer):
    token = serializers.SerializerMethodField()
    password = serializers.CharField(write_only=True)

    def get_token(self, obj):
        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
        payload = jwt_payload_handler(obj)
        token = jwt_encode_handler(payload)
        return token

    def create(self, validated_data):
        password = validated_data.pop('password', None)
        instance = self.Meta.model(**validated_data)
        if password is not None:
            instance.set_password(password)
        instance.last_login = timezone.now()
        instance.save()
        return instance

    class Meta:
        model = User
        fields = ('pk', 'token', 'username', 'email',
                  'password', 'first_name', 'last_name', 'iconUrl', 'description', 'phone')
