from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from .models import User


class AccountTests(APITestCase):

    def test_create_user(self):
        print("\n------------------------------Account Test----------------------------------------------------------------\n")
        """
        Ensure we can create a new User object.
        """
        url = reverse('register')
        data = {'username': 'testUser',
                'email': 'testtest@gmail.com', 'password': 'testtest'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(User.objects.get().username, 'testUser')
        print("Success: test_create_user")

    def test_wrong_email_format(self):
        """
        Test don't have correct email format
        """
        url = reverse('register')
        data = {'username': 'testUser',
                'email': 'testtestgmail.com', 'password': 'testtest'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(User.objects.count(), 0)
        print("\nSuccess: test_wrong_email_format")

    def test_exist_username(self):
        """
        Test the username already exist
        """
        url = reverse('register')
        data = {'username': 'testUser',
                'email': 'testtest1@gmail.com', 'password': 'testtest'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(User.objects.get().username, 'testUser')
        data = {'username': 'testUser',
                'email': 'testtest2@gmail.com', 'password': 'testtest'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(User.objects.count(), 1)
        print("\nSuccess: test_exist_username")

    def test_exist_email(self):
        """
        Test the email already exist
        """
        url = reverse('register')
        data = {'username': 'testUser1',
                'email': 'testtest@gmail.com', 'password': 'testtest'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(User.objects.get().username, 'testUser1')
        data = {'username': 'testUser2',
                'email': 'testtest@gmail.com', 'password': 'testtest'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(User.objects.count(), 1)
        print("\nSuccess: test_exist_email")

    def test_API_login(self):

        user = User.objects.create_user('username')
        user.set_password('Pas$w0rd')
        user.save()

        self.assertTrue(self.client.login(
            username='username', password='Pas$w0rd'))

    def test_response_data(self):
        """
        Test the response data
        """
        url = reverse('register')
        data = {'username': 'testUser',
                'email': 'testtest@gmail.com', 'password': 'testtest'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(User.objects.get().username, 'testUser')
        self.assertEqual(response.data['username'], 'testUser')
        self.assertEqual(response.data['email'], 'testtest@gmail.com')
        print("\nSuccess: test_response_data")

    def test_log_in(self):
        """
        Test user login
        """
        url = reverse('register')
        data = {'username': 'testUser',
                'email': 'testtest@gmail.com', 'password': 'testtest'}
        response1 = self.client.post(url, data, format='json')

        url = reverse('obtain_jwt_token')
        data = {'username': 'testUser',
                'email': 'testtest@gmail.com', 'password': 'testtest'}
        response2 = self.client.post(url, data, format='json')
        self.assertEqual(response2.data['username'], 'testUser')
        self.assertEqual(response1.data['token'], response2.data['token'])
        print("\nSuccess: test_log_in")

    def test_update_name(self):
        """
        Test updating the first and last name of the user
        """

        # register and log-in a user
        user = User.objects.create_user('username')
        user.set_password('password12345')
        user.save()

        self.assertTrue(self.client.login(
            username='username', password='password12345'))

        url = reverse('update_name')
        t1 = 'TEST_FIRST'
        t2 = 'TEST_LAST'
        # data = {'token': response.data.get('token'), 'first_name': t1, 'last_name': t2}

        data = {'first_name': t1, 'last_name': t2}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_202_ACCEPTED)

        url = reverse('current_user')
        response = self.client.get(url)
        self.assertEqual(response.data.get('first_name'), t1)
        self.assertEqual(response.data.get('last_name'),  t2)

    def test_update_email(self):
        """
        Test updating the first and last name of the user
        """

        # register and log-in a user
        preset_email = "this@is.test"
        user = User.objects.create_user('username', preset_email)
        user.set_password('password12345')
        user.save()

        self.assertTrue(self.client.login(
            username='username', password='password12345'))

        url = reverse('update_email')
        t = 'test@update.email'

        data = {'email': t}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_202_ACCEPTED)

        url = reverse('current_user')
        response = self.client.get(url)
        self.assertEqual(response.data.get('email'), t)

        # test update invalid email too
        url = reverse('update_email')
        t = 'invalidemail'

        data = {'email': t}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        url = reverse('current_user')
        response = self.client.get(url)
        self.assertEqual(response.data.get('email'), 'test@update.email')
