#!/bin/sh
# To clear up aircnc files and python virtualenv

SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")

echo "Warning: this script will delete everything including the current directory"
echo "by executing 'rm -rf \"$SCRIPTPATH\"'"
echo "[ Press ENTER to continue ]"

read confirmation

echo "You have 5 seconds to change your mind ..."
sleep 5

# quit virtualenv if still active 
deactivate

rm -rf "$SCRIPTPATH" && echo "Deleted."
