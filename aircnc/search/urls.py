from django.urls import path, include, re_path
from rest_framework import routers
from .views import search_houses, auto_complete

router = routers.DefaultRouter()

urlpatterns = [
    re_path(r'', include(router.urls)),
    re_path(r'^autocomplete', auto_complete.as_view()),
    re_path(r'^', search_houses.as_view()),
]
