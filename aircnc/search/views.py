from django.http import HttpResponseRedirect
from rest_framework import permissions, status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.views import APIView
from housing.models import Housing
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics
from .serializers import SearchHousingSerializer, AutoCompleteSerializer
from rest_framework.filters import SearchFilter, OrderingFilter
from django_filters import rest_framework as filters


class DateFilter(filters.FilterSet):
    startDate = filters.DateTimeFilter(
        field_name="startDate", lookup_expr='lte')
    endDate = filters.DateTimeFilter(
        field_name="endDate", lookup_expr='gte')

    class Meta:
        model = Housing
        fields = {
            'category': ['exact'],
            'price': ['exact', 'lte', 'gte'],
            'rating': ['exact', 'lte', 'gte'],
            'guests': ['exact', 'lte', 'gte'],
            'beds': ['exact', 'lte', 'gte'],
            'baths': ['exact', 'lte', 'gte'],
            'startDate': "",
            'endDate': ""
        }


class search_houses(generics.ListAPIView):
    permission_classes = (permissions.AllowAny,)
    queryset = Housing.objects.all()
    serializer_class = SearchHousingSerializer
    filter_backends = (SearchFilter,
                       DjangoFilterBackend, OrderingFilter,)
    filterset_class = DateFilter
    search_fields = ('listing_name', '=neighborhood')
    ordering_fields = ('listing_name', 'price', 'rating')

    ordering = ('listing_name')


class auto_complete(generics.ListAPIView):
    permission_classes = (permissions.AllowAny,)
    queryset = Housing.objects.values('neighborhood').distinct()
    serializer_class = AutoCompleteSerializer
    filter_backends = (SearchFilter, OrderingFilter,)
    search_fields = ('^neighborhood',)
    ordering = ('neighborhood',)
