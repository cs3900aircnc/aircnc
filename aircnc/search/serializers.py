from rest_framework import serializers
from housing.models import Housing, HouseImage
from account.models import User


class SearchHousingSerializer(serializers.ModelSerializer):
    images = serializers.StringRelatedField(many=True)

    class Meta:
        model = Housing
        fields = ('pk', 'neighborhood', 'price', 'beds', 'guests', 'baths', 'startDate', 'endDate',
                  'rating', 'listing_name', 'category', 'images')


class AutoCompleteSerializer(serializers.ModelSerializer):

    class Meta:
        model = Housing
        fields = ('neighborhood',)
        read_only_fields = ('neighborhood',)
