from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from account.models import User
from housing.models import Housing
from housing.models import HouseImage
from rest_framework_jwt.settings import api_settings
from django.utils import timezone
import datetime
import json


class SearchTests(APITestCase):

    def test_search(self):
        print("\n------------------------------Search Test----------------------------------------------------------------\n")
        """
        Test search by specific 
        """
        # register
        url = reverse('register')
        data = {'username': 'testUser',
                'email': 'testtest@gmail.com', 'password': 'testtest'}
        self.client.post(url, data, format='json')
        # login
        l = self.client.login(username='testUser', password='testtest')

        url = reverse('create_house')
        data = {
            "user": "1",
            "neighborhood": "Kingsford",
            "price": "333",
            "rules": "[No smoking, No pets]",
            "details": "{beds: 7, bedrooms: 7, guests: 7, baths: 7}",
            "address": "666 Anzac Parade",
            "listing_name": "Sydney property1",
            "listing_description": "Sydney property1",
            "category": "Apartment",
            "beds": "6",
            "guests": "6",
            "baths": "6",
            # "rating":"5.0",
            # "startDate":"datetime.datetime(2017, 10, 27, 11, 11, 11, 111111)",
            # "endDate":"datetime.datetime(2018, 10, 27, 11, 11, 11, 111111)"
            "startDate": "2017-10-27 00:00:00",
            "endDate": "2018-10-27 00:00:00"
        }
        response = self.client.post(url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        house = Housing.objects.get(pk=1)
        house.rating = 5.0
        house.save()

        data = {
            "user": "1",
            "neighborhood": "Kingsford",
            "price": "222",
            "rules": "[No smoking, No pets]",
            "details": "{beds: 7, bedrooms: 7, guests: 7, baths: 7}",
            "address": "666 Anzac Parade",
            "listing_name": "Sydney property2",
            "listing_description": "Sydney property2",
            "category": "Apartment",
            "beds": "4",
            "guests": "4",
            "baths": "4",
            # "rating":"2.0",
            # "startDate":"datetime.datetime(2017, 10, 27, 11, 11, 11, 111111)",
            # "endDate":"datetime.datetime(2018, 10, 27, 11, 11, 11, 111111)"
            "startDate": "2017-1-17 00:00:00",
            "endDate": "2018-2-17 00:00:00"
        }
        response = self.client.post(url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        house = Housing.objects.get(pk=2)
        house.rating = 2.0
        house.save()

        data = {
            "user": "1",
            "neighborhood": "Kingsford",
            "price": "111",
            "rules": "[No smoking, No pets]",
            "details": "{beds: 7, bedrooms: 7, guests: 7, baths: 7}",
            "address": "666 Anzac Parade",
            "listing_name": "Melbourne property1",
            "listing_description": "Melbourne property1",
            "category": "Apartment",
            "beds": "4",
            "guests": "4",
            "baths": "4",
            # "rating":"2.0",
            # "startDate":"datetime.datetime(2017, 10, 27, 11, 11, 11, 111111)",
            # "endDate":"datetime.datetime(2018, 10, 27, 11, 11, 11, 111111)"
            "startDate": "2017-1-17 00:00:00",
            "endDate": "2018-2-17 00:00:00"
        }
        response = self.client.post(url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        house = Housing.objects.get(pk=3)
        house.rating = 2.0
        house.save()

        data = {
            "user": "1",
            "neighborhood": "Kingsford",
            "price": "444",
            "rules": "[No smoking, No pets]",
            "details": "{beds: 7, bedrooms: 7, guests: 7, baths: 7}",
            "address": "666 Anzac Parade",
            "listing_name": "Melbourne property2",
            "listing_description": "Melbourne property2",
            "category": "Apartment",
            "beds": "3",
            "guests": "3",
            "baths": "3",
            # "rating":"1.0",
            # "startDate":"datetime.datetime(2017, 10, 27, 11, 11, 11, 111111)",
            # "endDate":"datetime.datetime(2018, 10, 27, 11, 11, 11, 111111)"
            "startDate": "2017-10-27 00:00:00",
            "endDate": "2018-10-27 00:00:00"
        }
        response = self.client.post(url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        house = Housing.objects.get(pk=4)
        house.rating = 1.0
        house.save()

        data = {
            "user": "1",
            "neighborhood": "Kingsford",
            "price": "555",
            "rules": "[No smoking, No pets]",
            "details": "{beds: 7, bedrooms: 7, guests: 7, baths: 7}",
            "address": "666 Anzac Parade",
            "listing_name": "Melbourne property3",
            "listing_description": "Melbourne property3",
            "category": "Apartment",
            "beds": "1",
            "guests": "1",
            "baths": "1",
            # "rating":"3.0",
            # "startDate":"datetime.datetime(2017, 10, 27, 11, 11, 11, 111111)",
            # "endDate":"datetime.datetime(2018, 10, 27, 11, 11, 11, 111111)"
            "startDate": "2017-10-27 00:00:00",
            "endDate": "2018-10-27 00:00:00"
        }
        response = self.client.post(url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        house = Housing.objects.get(pk=5)
        house.rating = 3.0
        house.save()

        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(User.objects.get().username, 'testUser')
        self.assertEqual(Housing.objects.count(), 5)
        # ////////////////////test_search_name////////////////////////////////////////////////////////////////
        response = self.client.get(
            '/search/Housing?search=Sydney', format='json')
        # print(response.json())
        self.assertEqual(len(response.json()), 2)
        response = self.client.get(
            '/search/Housing?search=Melbourne', format='json')
        # print(response.json())
        self.assertEqual(len(response.json()), 3)
        print(":::Success: test_search_name")

        # ////////////////////test_search_date////////////////////////////////////////////////////////////////
        response = self.client.get(
            '/search/Housing?startDate=2017-10-27&endDate=2018-10-27', format='json')
        # print(response.json())
        self.assertEqual(len(response.json()), 3)
        response = self.client.get(
            '/search/Housing?startDate=2017-1-17&endDate=2018-2-17', format='json')
        # print(response.json())
        self.assertEqual(len(response.json()), 2)
        print(":::Success: test_search_date")

        # ////////////////////test_search_rating////////////////////////////////////////////////////////////////
        response = self.client.get(
            '/search/Housing?rating__gte=3.5', format='json')
        # print(response.json())
        self.assertEqual(len(response.json()), 1)
        response = self.client.get(
            '/search/Housing?rating__lte=3.0', format='json')
        # print(response.json())
        self.assertEqual(len(response.json()), 4)
        print(":::Success: test_search_rating")

        # ////////////////////test_search_order////////////////////////////////////////////////////////////////
        response = self.client.get(
            '/search/Housing?ordering=price', format='json')
        # print(response.json())
        self.assertEqual(len(response.json()), 5)
        self.assertEqual(response.json()[0]['price'], 111)
        self.assertEqual(response.json()[1]['price'], 222)
        self.assertEqual(response.json()[2]['price'], 333)
        self.assertEqual(response.json()[3]['price'], 444)
        self.assertEqual(response.json()[4]['price'], 555)
        print(":::Success: test_search_ordering")

        # ////////////////////test_search_guest////////////////////////////////////////////////////////////////
        response = self.client.get(
            '/search/Housing?guests__gte=3', format='json')
        # print(response.json())
        self.assertEqual(len(response.json()), 4)
        response = self.client.get(
            '/search/Housing?guests__lte=4', format='json')
        # print(response.json())
        self.assertEqual(len(response.json()), 4)
        print(":::Success: test_search_guests")

        # ////////////////////test_search_bed////////////////////////////////////////////////////////////////
        response = self.client.get(
            '/search/Housing?beds__gte=3', format='json')
        # print(response.json())
        self.assertEqual(len(response.json()), 4)
        response = self.client.get(
            '/search/Housing?beds__lte=4', format='json')
        # print(response.json())
        self.assertEqual(len(response.json()), 4)
        print(":::Success: test_search_beds")

        # ////////////////////test_search_bath////////////////////////////////////////////////////////////////
        response = self.client.get(
            '/search/Housing?baths__gte=3', format='json')
        # print(response.json())
        self.assertEqual(len(response.json()), 4)
        response = self.client.get(
            '/search/Housing?baths__lte=4', format='json')
        # print(response.json())
        self.assertEqual(len(response.json()), 4)
        print(":::Success: test_search_baths")

        print("Success: test_search")
