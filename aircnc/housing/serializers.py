from rest_framework import serializers
from .models import Housing, HouseImage
from account.models import User
from booking.models import Booking
from account.serializers import UserSerializer
from booking.serializers import BookingDateRangeSerializer


class CreateHousingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Housing
        fields = ('pk', 'user', 'neighborhood', 'price', 'rules',
                  'address', 'listing_name', 'listing_description', 'category', 'beds', 'guests', 'baths', 'startDate', 'endDate')


class UpdateHousingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Housing
        fields = ('neighborhood', 'price', 'rules', 'address', 'listing_name', 'listing_description',
                  'category', 'beds', 'guests', 'baths', 'startDate', 'endDate')


class AddHousingImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = HouseImage
        fields = ('house', 'image')


class GetHousingSerializer(serializers.ModelSerializer):
    images = serializers.StringRelatedField(many=True)
    user = UserSerializer(read_only=True)
    bookings = serializers.SerializerMethodField()

    def get_bookings(self, house):
        bookings = Booking.objects.filter(status='A', house=house)
        serializer = BookingDateRangeSerializer(bookings, many=True)
        return serializer.data

    class Meta:
        model = Housing
        fields = ('pk', 'user', 'neighborhood', 'price', 'rules', 'address',
                  'listing_name', 'listing_description', 'rating', 'category', 'beds', 'guests', 'baths', 'startDate', 'endDate', 'images', 'bookings')


class GetHousingImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = HouseImage
        fields = ('house', 'image')
