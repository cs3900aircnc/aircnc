from django.http import HttpResponseRedirect
from rest_framework import permissions, status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.views import APIView
from .serializers import CreateHousingSerializer, GetHousingSerializer, GetHousingImageSerializer, AddHousingImageSerializer, UpdateHousingSerializer
from housing.models import Housing, HouseImage
from account.models import User
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import generics
from django.core import serializers
from urllib.parse import quote
import json
import requests as req


class my_houses(generics.ListAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = GetHousingSerializer

    def get_queryset(self):
        return Housing.objects.filter(user=self.request.user)


# TODO: create housing profile with mutiple images
class create_housing(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, format=None):
        request.data['user'] = request.user.id
        serializer = CreateHousingSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class addressAutoComplete(APIView):
    permission_classes = (permissions.AllowAny,)

    def get(self, requests, address, format=None):
        url = 'https://maps.googleapis.com/maps/api/place/autocomplete/json?input=' + quote(address) + \
            '&region=AU&key=AIzaSyC9J6R6ITBAEXTsHFCPnd4xU7xiCDYX-7Q'
        response = req.get(url)
        if response.status_code != 200:
            return Response(status=response.status_code)
        res = []
        for data in response.json()['predictions']:
            res.append(data['description'])
        return Response(res, status=response.status_code)


class replace_image(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, format=None):

        house = Housing.objects.filter(user=self.request.user, id=houseId)
        if not house.exists():
            error = {'error': 'You cannot add image for this house!'}
            return Response(error, status=status.HTTP_400_BAD_REQUEST)

        imgs = HouseImage.objects.filter(house=request.data.get('house'))
        if imgs.exists():
            for img in imgs:
                img.delete()

        for url in request.data.get('image'):
            data = {
                'house': request.data.get('house'),
                'image': url
            }
            serializer = AddHousingImageSerializer(data=data)
            if serializer.is_valid():
                serializer.save()
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        return Response(serializer.data, status=status.HTTP_200_OK)


class get_housing(APIView):
    permission_classes = (permissions.AllowAny,)

    def get(self, request, houseId, format=None):
        housings = Housing.objects.filter(id=houseId)
        if not housings.exists():
            error = {'error': 'The house(' + houseId + ') does not exists!'}
            return Response(error, status=status.HTTP_400_BAD_REQUEST)
        serializer = GetHousingSerializer(housings[0])
        return Response(serializer.data, status=status.HTTP_200_OK)


# TODO: Add mutiple images at same time -- OK
class add_image(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, houseId, format=None):
        house = Housing.objects.filter(user=self.request.user, id=houseId)
        if not house.exists():
            error = {'error': 'You cannot add image for this house!'}
            return Response(error, status=status.HTTP_400_BAD_REQUEST)

        data = request.data.get('image')
        image_list = []
        if type(data) == str:
            image_list.append(data)
        else:
            image_list = data

        # validate everything first before saving
        for image in image_list:
            data = {'house': houseId, 'image': image}
            serializer = AddHousingImageSerializer(data=data)
            # will return 404 if any image is invalid
            serializer.is_valid(raise_exception=True)

        imgs = HouseImage.objects.filter(house=houseId)
        if imgs.exists():
            imgs.delete()

        # save everything
        result = []
        for image in image_list:
            data = {'house': houseId, 'image': image}
            serializer = AddHousingImageSerializer(data=data)
            if serializer.is_valid():
                serializer.save()
                result.append(serializer.data)

        return Response(result, status=status.HTTP_201_CREATED)


class get_housing_image(APIView):
    permission_classes = (permissions.AllowAny,)

    def get(self, request, houseId, format=None):
        images = get_images_by_id(houseId)
        serializer = GetHousingImageSerializer(images, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class update_details(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, houseId, format=None):

        user = request.user
        try:
            house = Housing.objects.get(id=houseId)
        except:
            error = {
                'error': 'The house(' + houseId + ') does not exists!'
            }
            return Response(error, status=status.HTTP_400_BAD_REQUEST)

        # if key not found in request, use existing values instead
        # new_email = request.data.get('email', user.email)

        updated_data = {
            'neighborhood': request.data.get('neighborhood', house.neighborhood),
            'price': request.data.get('price', house.price),
            'rules': request.data.get('rules', house.rules),
            'address': request.data.get('address', house.address),
            'listing_name': request.data.get('listing_name', house.listing_name),
            'listing_description': request.data.get('listing_description', house.listing_description),
            'beds': request.data.get('beds', house.beds),
            'guests': request.data.get('guests', house.guests),
            'baths': request.data.get('baths', house.baths),
            'bedrooms': request.data.get('bedrooms', house.bedrooms),
            'category': request.data.get('category', house.category)
        }

        serializer = UpdateHousingSerializer(
            house, data=updated_data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_202_ACCEPTED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

# ######################### Helper Functions ###################################


def get_images_by_id(houseId):
    return HouseImage.objects.filter(house=houseId)

# A better solution found
# So all the helper functions below are not in use :( so sad
# All those functions might useful in search keep it for now


def get_images_for_houses(houses):
    i = 0
    houses = json.loads(serializers.serialize('json', houses))
    for house in houses:
        images = get_images_by_id(house['pk'])
        house['images'] = images_to_list(images)
        i += 1
    return houses


def images_to_list(images):
    i = 0
    imgs = {}
    for item in images:
        imgs[i] = item.image
        i += 1
    return [imgs]


def get_images_for_houses_(houses):
    i = 0
    print(len(houses))
    for i in range(len(houses)):
        images = get_images_by_id(houses[i].pk)
        houses[i].images = images_to_list(images)
    print(houses[2])
    return houses

# ######################### Api for test purpose ###################################
# We shounld't use those functions in production
# It's only for test purpose


class get_my_houses(generics.ListAPIView):
    permission_classes = (permissions.AllowAny,)

    queryset = Housing.objects.all()
    serializer_class = GetHousingSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('user',)
