from django.core.management.base import BaseCommand
from housing.models import Housing, HouseImage
from account.models import User
import os
import json
from random import randint

# Make sure you run makemigrations && migrate before running this
# Might want to make a copy of the old db first !
# copied from settings.py
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


class Command(BaseCommand):
    args = '<no args>'
    help = 'Ask wing for help :o'

    def _create_records(self):

        s = User.objects.all()
        if s is None or s == []:
            print("No user accounts found, create an admin or generate users please")
            exit(1)

        with open(BASE_DIR + "/data", 'r') as f:
            for i, line in enumerate(f.readlines()):
                # insert 3 houses for each user
                if i == 3 * len(s):
                    break
                user = s[i % len(s)]
                d = json.loads(line)
                if None in d.values():
                    continue
                insert = Housing(
                    user=user,
                    neighborhood=d['neighborhood'],
                    price=randint(100, 500),
                    number_of_reviews=d['number_of_reviews'],
                    rating=d['rating'],
                    rules=d['rules'],
                    beds=d['beds'],
                    guests=d['guests'],
                    baths=d['baths'],
                    bedrooms=d['bedrooms'],
                    address=d['address'],
                    listing_name=d['listing_name'],
                    listing_description=d['listing_description'],
                    startDate=d['available_dates'][0],
                    endDate=d['available_dates'][1],
                    category=''
                )

                insert.save()
                image_list = []
                for img in d['image']:
                    image = HouseImage(house=insert, image=img)
                    image.save()
                    image_list.append(str(image.id))

                print("For User({}) Inserted House({}) & HouseImage({})".format(
                    user.id, insert.id, ','.join(image_list)))

    def handle(self, *args, **options):
        self._create_records()
