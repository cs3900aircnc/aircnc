from django.db import models
# https://docs.djangoproject.com/en/2.1/topics/db/models/#models-across-files
# https://docs.djangoproject.com/en/2.1/ref/models/fields/#django.db.models.ForeignKey.on_delete

# Each Django model has a built-in primary key already, no need to declare our own


class Housing(models.Model):
    user = models.ForeignKey('account.User', default=None, null=True, on_delete=models.CASCADE,
                             verbose_name="owner_profile")

    neighborhood = models.CharField(max_length=64, default='')
    price = models.PositiveSmallIntegerField(default=0)
    number_of_reviews = models.PositiveSmallIntegerField(
        editable=False, default=0)
    rating = models.DecimalField(
        max_digits=2, decimal_places=1, editable=False, default=0)

    # stores a list in json.dumps() form
    rules = models.CharField(max_length=300, default='')

    address = models.CharField(max_length=200, default='')
    listing_name = models.CharField(max_length=200, default='')
    listing_description = models.TextField(max_length=500, default='')

    beds = models.PositiveSmallIntegerField(default=0)
    guests = models.PositiveSmallIntegerField(default=0)
    baths = models.PositiveSmallIntegerField(default=0)
    bedrooms = models.PositiveSmallIntegerField(default=0)

    # if you add any other fields like editable=False
    # then Django will complain about NOT NULL integrity error :(
    startDate = models.DateTimeField()
    endDate = models.DateTimeField()

    category = models.CharField(
        max_length=200, default='', null=True, blank=True)

    def __str__(self):
        return self.listing_name


class HouseImage(models.Model):
    house = models.ForeignKey('Housing', on_delete=models.CASCADE, related_name='images',
                              verbose_name="house_profile_images")
    image = models.URLField(default='')

    def __str__(self):
        return str(self.image)

    def __unicode__(self):
        return '%s' % (self.image)
