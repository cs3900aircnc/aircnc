from django.urls import path, include, re_path
from rest_framework import routers
from .views import create_housing, my_houses, get_my_houses, add_image, get_housing, get_housing_image, update_details, replace_image, addressAutoComplete

router = routers.DefaultRouter()

urlpatterns = [
    # testing endpoints
    re_path(r'^get_my_houses/$', get_my_houses.as_view()),
    # production endpoints
    re_path(r'', include(router.urls)),
    # re_path(r'(?i)^create/$', create_housing.as_view(),name='create_house'),
    path('create/', create_housing.as_view(), name='create_house'),
    # re_path(r'(?i)^my/$', my_houses.as_view()),
    path('my/', my_houses.as_view(), name='my_house'),
    re_path(r'(?i)^my/addimage/(?P<houseId>\d+)/$', add_image.as_view()),

    #re_path(r'(?i)^my/update_details/(?P<houseId>\d+)/$', update_details.as_view(), name='update_details'),
    re_path(r'^my/update_details/(?P<houseId>\d+)/$',
            update_details.as_view(), name='update_details'),
    path('replace_image/', replace_image.as_view()),
    #path('my/update_details/<int: houseId>/', update_details.as_view(), name='update_details'),
    re_path(r'(?i)^(?P<houseId>\d+)/$', get_housing.as_view()),
    re_path(r'(?i)^(?P<houseId>\d+)/images$', get_housing_image.as_view()),
    re_path(r'(?i)^addressAutoComplete/(?P<address>.*)$',
            addressAutoComplete.as_view())
]
