import requests
from geopy import distance
from rest_framework.views import APIView


google_api_key = "AIzaSyBQkRNlADh4vzMI4wYv32ekmlzLza2s5d0"


def get_lat_lng(address):
    api_response = requests.get(
        'https://maps.googleapis.com/maps/api/geocode/json?address={0}&key={1}'.format(address, google_api_key))
    api_response_dict = api_response.json()
    if api_response_dict['status'] == 'OK':
        latitude = api_response_dict['results'][0]['geometry']['location']['lat']
        longitude = api_response_dict['results'][0]['geometry']['location']['lng']
    print('Latitude:', latitude)
    print('Longitude:', longitude)
    return latitude, longitude


def get_distance(lat1, lng1, lat2, lng2):
    lat_lng1 = (lat1, lng1)
    lat_lng2 = (lat2, lng2)
    distance_km = round(distance.distance(lat_lng1, lat_lng2).km, 2)
    print('distance_km: ' + distance_km)
    return distance_km
