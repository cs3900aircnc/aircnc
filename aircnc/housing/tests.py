from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from account.models import User
from housing.models import Housing
from housing.models import HouseImage
from rest_framework_jwt.settings import api_settings
from django.forms.models import model_to_dict
from django.utils import timezone
import datetime
import json


class HousingTests(APITestCase):

    # If I can get fixtures working we can straight away use our sample data in tests too

    # remove clutter from functions
    test_data = {
        "user": 1,
        "neighborhood": "Kingsford",
        "price": 666,
        "rules": "[No smoking, No pets]",
        "address": "666 Anzac Parade",
        "listing_name": "TEST TEST TEST",
        "listing_description": "Entire home/apt in Earlwood, Australia. Welcome to our renovated, newly furnished, two-story house in Earlwood, carefully furnished to ensure your comfort. Earlwood is a quiet and peaceful Sydney local area. Walk around you will find creeks, golf clubs, parks etc..  There are 6 bedroom",
        "category": "Apartment",
        "beds": 6,
        "bathrooms": 6,
        "guests": 6,
        "baths": 6,
        "startDate": "2017-10-27 06:00:00",
        "endDate": "2018-10-27 06:00:00"
    }

    maxDiff = None

    # just use force login when you're not explicitly testing the user info
    user_data = {'username': 'testUser',
                 'email': 'testtest@gmail.com', 'password': 'testtest'}

    def test_create_house(self):
        """
        Test get current user's houses
        """

        self.client.force_login(
            User.objects.get_or_create(username='testUser')[0])

        url = reverse('create_house')
        # we need to assign it this way otherwise it is just a referrence to the data
        data = dict(self.test_data)
        # and any changes we make to data will also affect self.test_data

        response = self.client.post(url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(User.objects.get().username, 'testUser')
        self.assertEqual(Housing.objects.count(), 1)
        print("\nSuccess: test_create_house")

    def test_incomplete_data(self):

        self.client.force_login(
            User.objects.get_or_create(username='testUser')[0])

        url = reverse('create_house')
        incomplete = dict(self.test_data)
        incomplete['listing_name'] = ""     # leave it empty to test validators

        response = self.client.post(url, data=incomplete, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Housing.objects.count(), 0)
        print("\nSuccess: test_incomplete_data")

    def test_my_housing(self):
        """
        Test get current user's houses
        """
        self.client.force_login(
            User.objects.get_or_create(username='testUser')[0])

        url = reverse('create_house')
        response = self.client.post(url, data=self.test_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        # same as previous data but incremented userID
      
        data2 = dict(self.test_data)

        response2 = self.client.post(url, data=data2, format='json')

        # create another house ,user=1
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(User.objects.get().username, 'testUser')
        self.assertEqual(Housing.objects.count(), 2)

        url = reverse('my_house')
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Housing.objects.filter(user=1).count(), 2)
        self.assertEqual(len(response.data), 2)
        print("\nSuccess: test_my_housing")

    def test_add_image(self):
        print("\n------------------------------Housing Test----------------------------------------------------------------\n")
        """
        Test add image to house
        """
        # register and login
        self.client.force_login(
            User.objects.get_or_create(username='testUser')[0])

        # add house
        url = reverse('create_house')
        data = dict(self.test_data)
        self.client.post(url, data=data, format='json')

        data = {
            "house": 1,
            "image": "http://static.messynessychic.com/wp-content/uploads/2013/01/20130104-130022.jpg"
        }

        response = self.client.post(
            '/rooms/my/addimage/1/', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(HouseImage.objects.count(), 1)
        self.assertEqual(HouseImage.objects.filter(house=1).count(), 1)

        print("Success: test_add_image")

    def test_add_multiple_images(self):

        # register and login
        self.client.force_login(
            User.objects.get_or_create(username='testUser')[0])

        # add house
        url = reverse('create_house')
        data = dict(self.test_data)
        self.client.post(url, data=data, format='json')

        data = {
            "house": 1,
            "image": [
                "http://static.messynessychic.com/wp-content/uploads/2013/01/20130104-130022.jpg",
                "https://images.unsplash.com/photo-1502672260266-1c1ef2d93688",
                "https://images.unsplash.com/photo-1467987506553-8f3916508521",
                "https://images.unsplash.com/photo-1533008093099-e2681382639a",
                "https://images.unsplash.com/photo-1501700142483-1fb4ec23aa4c"
            ]

        }

        response = self.client.post(
            '/rooms/my/addimage/1/', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(HouseImage.objects.count(), 5)
        self.assertEqual(HouseImage.objects.filter(house=1).count(), 5)

        data['image'][0] = "blablablab"  # invalidate the data
        response = self.client.post(
            '/rooms/my/addimage/1/', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(HouseImage.objects.count(), 5)
        self.assertEqual(HouseImage.objects.filter(house=1).count(), 5)

    def test_get_housing_profile(self):
        """
        Test get housing profile by Housing pk
        """
        # register and login
        self.client.force_login(
            User.objects.get_or_create(username='testUser')[0])

        # add house
        url = reverse('create_house')
        data = dict(self.test_data)
        response = self.client.post(url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        # get the info
        response = self.client.get('/rooms/1/', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Housing.objects.count(), 1)

        print("\nSuccess: test_get_housing_profile")

    def test_get_images(self):
        """
        Test add image to house
        """
        # register and login
        self.client.force_login(
            User.objects.get_or_create(username='testUser')[0])

        # add house
        url = reverse('create_house')
        data = dict(self.test_data)
        self.client.post(url, data=data, format='json')

        # add first image
        data = {
            "house": 1,
            "image": "http://static.messynessychic.com/wp-content/uploads/2013/01/20130104-130022.jpg"
        }
        response = self.client.post(
            '/rooms/my/addimage/1/', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        # add second image
        data = {
            "house": 1,
            "image": [
                "http://static.messynessychic.com/wp-content/uploads/2013/01/20130104-130021.jpg",
                "http://static.messynessychic.com/wp-content/uploads/2013/01/20130104-130022.jpg"
            ]
        }

        response = self.client.post(
            '/rooms/my/addimage/1/', data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(HouseImage.objects.count(), 2)
        self.assertEqual(HouseImage.objects.filter(house=1).count(), 2)

        response = self.client.get('/rooms/1/images', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.json()), 2)

        print("\nSuccess: test_get_images")

    def test_update_info(self):
        """
        Test updatinng an existing image
        """
        # register and login
        self.client.force_login(
            User.objects.get_or_create(username='testUser')[0])

        # add house
        url = reverse('create_house')
        data = dict(self.test_data)
        response = self.client.post(url, data=data, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Housing.objects.count(), 1)

        # update the details (correct)
        url = reverse('update_details', kwargs={
                      'houseId': response.data.get('pk')})

        data['listing_description'] = "[SOLD OUT] " + \
            data['listing_description']
        data['beds'] = 5
        data['category'] = "Cabin"

        # update created
        response = self.client.post(url, data=data, format='json')
        self.assertEqual(response.status_code, status.HTTP_202_ACCEPTED)
        self.assertIn("[SOLD OUT]", response.data.get('listing_description'))
        self.assertEqual(Housing.objects.count(), 1)

        # update the details (user error)
        missing_data = dict(data)
        missing_data['listing_description'] = ""    # obviously raises an error
        response = self.client.post(url, data=missing_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Housing.objects.count(), 1)

        # check that this field hasn't been updated
        self.assertEqual(model_to_dict(Housing.objects.all().first())[
                         'listing_description'], data['listing_description'])
