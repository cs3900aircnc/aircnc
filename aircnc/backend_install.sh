#!/bin/bash
set -e      # stop execution upon error 

cecho() { 
    echo "`tput setaf 1`$1`tput sgr0`" 
}

gecho() { 
    echo "`tput setaf 2`$1`tput sgr0`" 
}

SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")

[ ! -f "manage.py" ] && echo "This script should be placed in the same folder as manage.py!" && exit 1
[ ! -f "requirements.txt" ] && echo "This script should be placed in the same folder as requirements.txt!" && exit 1


cecho "| Setting up python virtualenv in $SCRIPTPATH"

which virtualenv > /dev/null || (echo "Virtualenv not installed, run [pip3 install virtualenv] and re-run the script" && exit 1)

virtualenv -q -p python3 env
source env/bin/activate

cecho "| Installing requirements"
pip install --no-cache-dir -r requirements.txt

cecho "| Setting up node virtualenv"
nodeenv -p
which node > /dev/null && echo "OK"


cecho "| Resetting database file"
rm -f db.sqlite3

cecho "| Migrating changes to database file"
python manage.py makemigrations && python manage.py migrate
[ -f 'db.sqlite3' ] && echo "OK" 

cecho "| Populating database file with data"
python manage.py loaddata output.json

cecho "| Run the server"
python manage.py runserver &

sleep 3     # let the stderr output appear first 
cecho "| SETUP COMPLETE"
gecho "[>] To stop the server: [pkill -f runserver]"
gecho "[>] To start the server: [python manage.py runserver]"
gecho "[>] To exit the virtualenv: [deactivate]"
gecho "[>] To activate the virtualenv: [source env/bin/activate]"

